import telebot #https://github.com/eternnoir/pyTelegramBotAPI
import pandas as pd
import csv
from csv import DictReader
import matplotlib.pyplot as plt
import logging

with open('/home/pi/Documents/CoronaVStatsBOT_key/key.txt', 'r') as tlg_key: #Telegram Bot Key
    key = tlg_key.read()

bot = telebot.TeleBot(key)

#message handler which handles incoming /start and /help commands
@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.reply_to(message, 'Bot under development, you can test it sending any country written correctly in English with a capital letter, without /. \n \nSend /countries to access the list of available countries or /help for help. \nTelegram contact: @retiolus \nEmail contact: coronavstatsbot@protonmail.com \n \Help me improve this bot: https://gitlab.com/retiolus/telegram-corona-bot/')

@bot.message_handler(commands=['countries'])
def countries(message):
    with open("data/coronavirus_data.csv", "r") as csv_file: #open file where all countries data is
        list = [row["Country,Other"] for row in DictReader(csv_file)] #makes a list of all actual countries in "Country,Other" column
    bot.reply_to(message, ', '.join(list)) #reply the countries list

#this function handles all incoming non commands messages
@bot.message_handler(regexp='')
def handle_message(message):
    
    startmessage = message.text #we assign message.txt (incoming message) to a variable
    chat_id = message.chat.id #we assign the Telegram message chat id to a variable
	
    keywords = {startmessage}
    print(keywords)

    #makes a list of all actual countries in worldometers data
    with open("data/coronavirus_data.csv", "r") as csv_file1:
        wdm_list = [row["Country,Other"] for row in DictReader(csv_file1)]

    #makes a list of all actual countries in Wikipedia data
    with open("data/world_data.csv", "r") as csv_file2:
        wiki_list = [row["Country (or dependent territory)"] for row in DictReader(csv_file2)]

    if startmessage in wdm_list and startmessage in wiki_list: #if startmessage is in bouth worldometers and Wikipedia lists, some countries are not written the same way and the name must be changed manually. ex: USA in worldometers list and United States in Wikepedia list. 
    
        #extract specific country data in all worldometers data and save it as a new country_data.csv file
        df = pd.read_csv("data/coronavirus_data.csv", sep=",")
        output = pd.DataFrame(columns=df.columns)
        for i in range(len(df.index)):
            if any(x in df['Country,Other'][i] for x in keywords):
                output.loc[len(output)] = [df[j][i] for j in df.columns]
        output.to_csv("data/coronavirus_country_data.csv", index=False)
        df = pd.read_csv("data/coronavirus_country_data.csv", sep=",")
        with open('data/coronavirus_country_data.csv', 'r') as f:
            reader = csv.reader(f)
            #read file row by row
        #    for column in reader:
        #        print (column)
        
        #extract specific country data in all Wikipedia data and save it as a new country_data.csv file
        df = pd.read_csv("data/world_data.csv", sep=",")
        output = pd.DataFrame(columns=df.columns)
        for i in range(len(df.index)):
            if any(x in df['Country (or dependent territory)'][i] for x in keywords):
                output.loc[len(output)] = [df[j][i] for j in df.columns]
        output.to_csv("data/world_country_data.csv", index=False)
        df = pd.read_csv("data/world_country_data.csv", sep=",")
        with open("data/world_country_data.csv", "r") as f:
            reader = csv.reader(f) 
        #    for row in reader:
        #        print (row)
        
        df = pd.read_csv("data/coronavirus_country_data.csv", sep=",") #read worldometers country data
        totalcases=(df.iloc[0,3])
        activecases=(df.iloc[0,8])
        totalactive=((activecases*100)/totalcases)
        read_pop = pd.read_csv("data/world_country_data.csv", sep=",") #read Wikipedia country population data
        country_pop = int(read_pop.iloc[0,3])
        p_pop_infected = float((activecases*100)/country_pop)
        p_pop_infected = float("{:.5f}".format(p_pop_infected)) #limit to 5 decimals
        #print(country_pop)
        
        #message that the bot is going to reply to user with all data
        infomessage = '🌍 Country: ' + str(df.iloc[0,2]) + '\n\n💉 Tests/1M pop: ' + str(df.iloc[0,13]) + '\n\n☣️ Total Cases: ' + str(df.iloc[0,3])+ '\nNew Cases: ' +  str(df.iloc[0,4]) + '\nActive Cases: ' + str(df.iloc[0,8]) + '\n\n☠️ Total Deaths: ' + str(df.iloc[0, 5]) + '\nNew Deaths: ' + str(df.iloc[0,6]) + '\n\n💊 Total Recovered: ' + str(df.iloc[0,7]) + '\n\n❗️ Serious, Critical: ' + str(df.iloc[0,9]) + '\n\n⚠️ % of people infected at the moment on the total since the beginning: ' + str(totalactive) + '\n\n📊 % of people actually infected on the total population of the country: ' + str(p_pop_infected)
        
        bot.reply_to(message, infomessage)
        
        with open("log.txt", "a") as file:
            file.write(startmessage + "\n")

    else:
        bot.reply_to(message, 'This is not a correct country. Send /countries to access the list of available countrie')

bot.polling()
