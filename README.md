# telegram-corona-bot

Telegram bot to get the latest data of the number of Coronavirus cases in a country through Telegram, 
data collected on https://www.worldometers.info/coronavirus/

Before launching the tlginteract.py file that manages the Telegram bot, you must run the coronavirus_data.py and world_data.py files.
The first collects data on https://www.worldometers.info/coronavirus/ and the second on https://en.wikipedia.org/wiki/List_of_countries_by_population . Some city names do not match in both pages (for example "USA" in Worldometers and "United States" in Wikipedia), they must be changed manually.

After that we can run tlginteract.py file.

I run stats_files.py once a day (at 01:05 CEST).

All data is saved in a same "data" folder that you must create in /telegram-corona-bot directory.

![Screenshot](photo_2020-04-28_15-21-01.jpg)