import pandas as pd
import csv
from csv import DictReader
import os.path
from datetime import datetime

with open("data/coronavirus_data.csv", "r") as csv_file1: #path where all countries data is saved
    wdm_list = [row["Country,Other"] for row in DictReader(csv_file1)] #create a list with all countries names

for y in wdm_list:
    path ="data/stats_data/" + y + ".csv" #file path
#    path_list = [path] 
#    print(path_list)
    if os.path.isfile(path) != True: #if file doesn't exists:
        open("data/stats_data/" + y + ".csv", "w+") #create new csv file with country name
        f = open (path, "a") #open file were to write new data
        f.write("{}\n".format("Date,Active Cases"))
        print("New file: " + y)
    
for y in wdm_list: 
    path ="data/stats_data/" + y + ".csv" #file path
    country = y
    df = pd.read_csv("data/coronavirus_data.csv", sep=",") #read all countries data 
    output = pd.DataFrame(columns=df.columns)   
    for i in range(len(df.index)):
        if any(x in df['Country,Other'][i] for x in {country}):
            output.loc[len(output)] = [df[j][i] for j in df.columns]
    final_data = [output.to_csv("data/stats_data/" + y + "_daydata.csv", index=False)]
    
    daydata = pd.read_csv("data/stats_data/" + country + "_daydata.csv", sep=",")
    activecases=int(daydata.iloc[0,7])
    with open(path, 'a', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',')
        spamwriter.writerow([datetime.now().strftime('%Y-%m-%d')] + [activecases])
#    f = open (path, "a") #open file were to write new data
#    f.write("{}\n".format(final_data)) #write new data in a new row
    
            #output.to_csv("new_data.csv", index=False)
            
#f = open (country + ".csv", "a")
#f.write("{}\n".format("10"))
