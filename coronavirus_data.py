import requests
import pandas as pd

url = 'https://www.worldometers.info/coronavirus/#nav-today/'
html = requests.get(url).content
df_list = pd.read_html(html)
df = df_list[-2]
print(df)

df.to_csv('data/coronavirus_data.csv')
df.to_html('data/coronavirus_data.htm')
