import requests
import pandas as pd

url = 'https://en.wikipedia.org/wiki/List_of_countries_by_population'
html = requests.get(url).content
df_list = pd.read_html(html)
df = df_list[-2]
print(df)

df.to_csv('data/world_data.csv')
df.to_html('data/world_data.htm')
