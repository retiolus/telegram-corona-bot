from csv import DictReader
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(16,9)) # Setting the figure size

with open("data/stats_data/France.csv", "r") as csv_file1: #path where country data is saved
    X = [row["Date"] for row in DictReader(csv_file1)] #create a list with all dates
    print(X)
    
with open("data/stats_data/France.csv", "r") as csv_file2: #path where country data is saved
    Y = [row["Active Cases"] for row in DictReader(csv_file2)] #create a list with all dates
    print(Y)

plt.plot(X, Y, label = "Active Cases")

plt.xlabel('Date') # Labeling the X-axis 
plt.ylabel('Active Cases') # Labeling the Y-axis 

plt.title('Active Cases Evolution') #graph title

# Show a legend on the plot 
plt.legend() 

fig.savefig('data/country_graph.png', bbox_inches='tight', dpi=250) #save the plot as an image 

#Showing the plot
#plt.show() 
